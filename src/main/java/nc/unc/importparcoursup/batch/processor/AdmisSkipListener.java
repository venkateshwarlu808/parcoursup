package nc.unc.importparcoursup.batch.processor;

import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRejetRepository;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidat;
import nc.unc.importparcoursup.model.AdmisSkipException;
import nc.unc.importparcoursup.utils.MapperUtils;
import org.springframework.batch.core.SkipListener;

public class AdmisSkipListener implements SkipListener<Admis, PreCandidat> {

    private AdmisRejetRepository admisRejetRepository;

    public AdmisSkipListener(AdmisRejetRepository admisRejetRepository) {
        this.admisRejetRepository = admisRejetRepository;
    }

    @Override
    public void onSkipInRead(Throwable t) {
        throw new RuntimeException("onSkipInRead: Code shouldn't reach here");
    }

    @Override
    public void onSkipInWrite(PreCandidat item, Throwable t) {
        throw new RuntimeException("onSkipInWrite: Code shouldn't reach here");
    }

    @Override
    public void onSkipInProcess(Admis item, Throwable t) {
        if (t instanceof AdmisSkipException) {
            admisRejetRepository.save(MapperUtils.getAdmisRejetFromAdmisSkipException((AdmisSkipException)t));
        } else {
            throw new RuntimeException(t);
        }
    }
}
