package nc.unc.importparcoursup.view.informationrules;

import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import org.apache.commons.lang3.time.DateFormatUtils;

public class LastExecDateIE extends InformationElement<AdmisHistory> {
    private static final long serialVersionUID = 1L;

    @Override
    public String updateValue(AdmisHistory item) {
        return DateFormatUtils.format(item.getJobExecutionDate(), "dd/MM/yyy HH:mm");
    }

    @Override
    protected String getLabel() {
        return "Dernier Job - Date";
    }

}
