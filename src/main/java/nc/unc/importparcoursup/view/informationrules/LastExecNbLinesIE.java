package nc.unc.importparcoursup.view.informationrules;

import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;

public class LastExecNbLinesIE extends InformationElement<AdmisHistory> {
    private static final long serialVersionUID = 1L;

    @Override
    public String updateValue(AdmisHistory item) {
        return "" + item.getAdmis().size();
    }

    @Override
    protected String getLabel() {
        return "Dernier Job - Lignes importées";
    }

}
