package nc.unc.importparcoursup.view.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import nc.unc.importparcoursup.utils.SpecialCharUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class Stepper {

    private Step activeStep;
    private Map<Step, Pair<Label, Component>> components = new HashMap<Step, Pair<Label, Component>>();

    public Stepper(String... labelParams) {
        IntStream.range(0, labelParams.length).forEach(i -> {
            Step currentStep = Step.values()[i + 1];
            HorizontalLayout stepComponent = new HorizontalLayout();
            stepComponent.setClassName("step");
            Label number = new Label(updateLabelByIndex(currentStep));
            number.setClassName("number-step");
            stepComponent.add(number);
            stepComponent.add(new Label(labelParams[i]));
            components.put(currentStep, Pair.of(number, stepComponent));
        });

    }

    public Component getStepComponent(Step step) {
        return components.get(step).getRight();
    }

    public Step getCurrentStep() {
        return activeStep;
    }

    public void setStep(Step activeStep) {
        this.activeStep = activeStep;
        components.keySet().forEach(
                step -> components.get(step).getLeft().setText(step.value <= activeStep.value ? SpecialCharUtils.CHECKED : updateLabelByIndex(step))
        );
    }

    private String updateLabelByIndex(Step step) {
        switch (step) {
            case STEP_ONE:
                return SpecialCharUtils.CIRCLE_ONE;
            case STEP_TWO:
                return SpecialCharUtils.CIRCLE_TWO;
            case STEP_THREE:
                return SpecialCharUtils.CIRCLE_THREE;
            default:
                return "-";
        }
    }

    public enum Step {
        STEP_NONE(0), STEP_ONE(1), STEP_TWO(2), STEP_THREE(3);
        private final int value;

        Step(int value) {
            this.value = value;
        }
    }
}

