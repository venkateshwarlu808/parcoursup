package nc.unc.importparcoursup.view.verificationrules;

import nc.unc.importparcoursup.utils.MyFileUtils;
import nc.unc.importparcoursup.view.components.UploadComponent;

public class FileIsNotEmpty extends CheckElement {
    private static final long serialVersionUID = 1L;

    public FileIsNotEmpty(UploadComponent upload) {
        super(upload);
    }

    @Override
    public boolean isValidCheck() {
        return !MyFileUtils.isEmpty(uploadComponent.getFile().get());
    }

    @Override
    protected String getLabel() {
        return "Fichier non vide";
    }

}
