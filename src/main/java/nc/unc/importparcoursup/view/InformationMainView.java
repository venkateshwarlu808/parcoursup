package nc.unc.importparcoursup.view;

import com.opengamma.strata.collect.Unchecked;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import nc.unc.importparcoursup.Application;
import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import nc.unc.importparcoursup.dao.admisDAO.AdmisRejet;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisHistoryRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRejetRepository;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Stream;

@Route(layout = TopBarLayout.class, value = "secured/informations")
@PageTitle(Application.APP_NAME)
public class InformationMainView extends HorizontalLayout {
    static final long serialVersionUID = 1L;
    private Grid<AdmisRejet> gridRejet = new Grid<>();
    private Grid<Entry<String, String>> admisPropertiesGrid = new Grid<>();

    public InformationMainView(@Autowired AdmisHistoryRepository admisHistoryRepository,
                               @Autowired AdmisRejetRepository admisRejetRepository) {
        VerticalLayout leftContent = new VerticalLayout();
        VerticalLayout middleContent = new VerticalLayout();
        VerticalLayout rightContent = new VerticalLayout();
        rightContent.add(admisPropertiesGrid);

        // init grids
        Grid<AdmisHistory> gridJobHistory = getAdmisHistoryGrid(admisHistoryRepository, admisRejetRepository);
        gridRejet.addColumn(AdmisRejet::getTypeRejet).setHeader("Type rejet");
        gridRejet.addColumn(AdmisRejet::getInformation).setHeader("Valeur");
        admisPropertiesGrid.addColumn(Entry::getKey).setHeader("Clé");
        admisPropertiesGrid.addColumn(Entry::getValue).setHeader("Valeur");

        // contents
        leftContent.add(gridJobHistory);
        middleContent.add(gridRejet);
        add(leftContent, middleContent, rightContent);

        // listeners
        gridJobHistory.asSingleSelect().addValueChangeListener(
                event -> updateHistoryDetail(event.getValue(), admisRejetRepository));
        gridRejet.asSingleSelect().addValueChangeListener(
                event -> updateRejetDetail(event.getValue()));
    }

    private void updateHistoryDetail(AdmisHistory admisHistory, AdmisRejetRepository admisRejetRepository) {
         gridRejet.setItems(admisRejetRepository.findAdmisRejetByAdmisHistory(admisHistory));
    }

    private void updateRejetDetail(AdmisRejet rejet) {
        if (rejet != null) {
            Unchecked.wrap(() -> {
                Admis admis = rejet.getAdmis();
                Map<String, String> properties = BeanUtils.describe(admis);
                TreeMap<String, String> sortedProperties = new TreeMap<>(properties);
                admisPropertiesGrid.setItems(sortedProperties.entrySet());
            });
        } else {
            admisPropertiesGrid.setItems(Stream.empty());
        }
    }


    private Grid<AdmisHistory> getAdmisHistoryGrid(AdmisHistoryRepository admisHistoryRepository, AdmisRejetRepository admisRejetRepository) {
        Grid<AdmisHistory> grid = new Grid<>();
        grid.setItems(IteratorUtils.toList(admisHistoryRepository.findAll().iterator()));
        grid.addColumn(AdmisHistory::getJobExecutionDate).setHeader("Date Importation");
        grid.addColumn(AdmisHistory::getCreatedBy).setHeader("Par");
        grid.addColumn(AdmisHistory::getUai).setHeader("UAI");
        grid.addColumn(admisHistory -> admisRejetRepository.findAdmisRejetByAdmisHistory(admisHistory).size()).setHeader("Nb Rejets");
        return grid;
    }
}
