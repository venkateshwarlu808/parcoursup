package nc.unc.importparcoursup.model;

public enum TypeRejet {
    CODE_DIPLOME_INCONNU, CODE_DIPLOME_EN_DOUBLE, CODE_BAC_INCONNU(false), CODE_PAYS_INCONNU;
    private boolean blocking;

    TypeRejet() {
        this.blocking = true;
    }

    TypeRejet(boolean blocking) {
        this.blocking = blocking;
    }

    public boolean isBlocking() {
        return blocking;
    }

    public void setBlocking(boolean blocking) {
        this.blocking = blocking;
    }
}
