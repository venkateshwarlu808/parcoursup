package nc.unc.importparcoursup.dao.candidatDAO.repository;

import nc.unc.importparcoursup.dao.candidatDAO.Candidat;
import org.springframework.data.repository.CrudRepository;

public interface CandidatRepository extends CrudRepository<Candidat, Long> {
}
