package nc.unc.importparcoursup.dao;

import nc.unc.importparcoursup.model.UAI;

public interface IHistoryRepository {
    IHistory findTopByUaiOrderByJobExecutionDateDesc(UAI uai);

    IHistory findFirstByFileCheckSum(String checkSum);
}
