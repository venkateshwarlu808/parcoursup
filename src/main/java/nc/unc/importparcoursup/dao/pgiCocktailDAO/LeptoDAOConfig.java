package nc.unc.importparcoursup.dao.pgiCocktailDAO;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Objects;

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "leptoEntityManagerFactory", transactionManagerRef = "leptoTransactionManager")
public class LeptoDAOConfig {

    @Value("${hibernate.hbm2ddl.auto}")
    private String ddlMode;

    @Value("${datasource.pgi-cocktail.show-sql}")
    private boolean showsql;

    @Bean
    public PlatformTransactionManager leptoTransactionManager() {
        return new JpaTransactionManager(Objects.requireNonNull(leptoEntityManagerFactory().getObject()));
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean leptoEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(leptoDataSource());
        HibernateJpaVendorAdapter vendor = new HibernateJpaVendorAdapter();
        vendor.setShowSql(showsql);
        factoryBean.setJpaVendorAdapter(vendor);
        factoryBean.setPackagesToScan(LeptoDAOConfig.class.getPackage().getName());
        HashMap<String, Object> properties = new HashMap<>();
        // nver touch the cocktail schema
        properties.put("hibernate.hbm2ddl.auto", ddlMode);
        factoryBean.setJpaPropertyMap(properties);
        return factoryBean;
    }

    @Bean
    @ConfigurationProperties(prefix = "datasource.pgi-cocktail")
    public DataSource leptoDataSource() {
        return DataSourceBuilder.create().build();
    }

}