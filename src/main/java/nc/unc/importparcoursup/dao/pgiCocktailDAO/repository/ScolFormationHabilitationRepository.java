package nc.unc.importparcoursup.dao.pgiCocktailDAO.repository;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.ScolFormationHabilitation;
import org.springframework.data.repository.CrudRepository;

public interface ScolFormationHabilitationRepository extends CrudRepository<ScolFormationHabilitation, Long> {
}
