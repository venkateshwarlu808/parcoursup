package nc.unc.importparcoursup.dao.pgiCocktailDAO.repository;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.Pays;
import org.springframework.data.repository.CrudRepository;

public interface PaysRepository extends CrudRepository<Pays, Long> {
    boolean existsByCodepays(String codepays);
}
