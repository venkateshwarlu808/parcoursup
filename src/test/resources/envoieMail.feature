Feature: Intégration des nouveaux précandidats via le job

  Scenario: Nouveau préCandidat -> envoi d'un mail
    Given Les codes pays
      | PAYS_CODE |
      | 100       |
    Given Les codes bac
      | BAC_CODE |
      | S        |
    Given Les codes diplomes
      | FSPN_KEY | FDIP_CODE |
      | 1        | 11        |
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode | PaysCodeNais |
      | 100        |    11    | john | S       | 100      |
    When Lancer le job integration parcoursup
    Then 1 email(s) recut

  Scenario: MAJ d'un préCandidat -> pas de mail lors de la mise jour
    Given Les codes pays
      | PAYS_CODE |
      | 100       |
    Given Les codes bac
      | BAC_CODE |
      | S        |
    Given Les codes diplomes
      | FSPN_KEY | FDIP_CODE |
      | 1        | 11        |
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode |
      | 100        | 11       | john | S       |
    When Lancer le job integration parcoursup
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode | PaysCodeNais |
      | 100        | 11       | jack | S       | 100          |
    When Lancer le job integration parcoursup
    Then 1 email(s) recut

  Scenario: 2 nouveaux préCandidats -> 2 mails
    Given Les codes pays
      | PAYS_CODE |
      | 100       |
    Given Les codes bac
      | BAC_CODE |
      | S        |
    Given Les codes diplomes
      | FSPN_KEY | FDIP_CODE |
      | 1        | 11        |
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode |
      | 100        | 11       | john | S       |
    When Lancer le job integration parcoursup
    Then 1 email(s) recut
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode | PaysCodeNais |
      | 101        | 11       | jack | S       | 100          |
    When Lancer le job integration parcoursup
    Then 2 email(s) recut