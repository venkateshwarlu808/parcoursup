Feature: Intégration des précandidats en rejet via le job

  Scenario: Rejet code diplome inconnu
    Given Les codes pays
      | PAYS_CODE |
      | 100       |
    Given Les codes bac
      | BAC_CODE |
      | S        |
    Given Les codes diplomes
      | FSPN_KEY | FDIP_CODE |
      | 1        | 22        |
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode | PaysCodeNais |
      | 100        |    11    | john | S       | 100      |
    When Lancer le job integration parcoursup
    Then Affiche les tables
    Then Il y a 0 precandidat(s) dans la base precandidat
    Then Admis 100 est en rejet CODE_DIPLOME_INCONNU

  Scenario: Rejet code bac inconnu
    Given Les codes pays
      | PAYS_CODE |
      | 100       |
    Given Les codes bac
      | BAC_CODE |
      | S        |
    Given Les codes diplomes
      | FSPN_KEY | FDIP_CODE |
      | 1        | 22        |
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode | PaysCodeNais |
      | 100        |    22    | john | A       | 100      |
    When Lancer le job integration parcoursup
    Then Affiche les tables
    Then Il y a 0 precandidat(s) dans la base precandidat
    Then Admis 100 est en rejet CODE_BAC_INCONNU

  Scenario: Rejet code pays inconnu
    Given Les codes pays
      | PAYS_CODE |
      | 100       |
    Given Les codes bac
      | BAC_CODE |
      | S        |
    Given Les codes diplomes
      | FSPN_KEY | FDIP_CODE |
      | 1        | 22        |
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode | PaysCodeNais |
      | 100        |    22    | john | S       | 101          |
    When Lancer le job integration parcoursup
    Then Affiche les tables
    Then Il y a 1 precandidat(s) dans la base precandidat
    Then Admis 100 est en rejet CODE_PAYS_INCONNU