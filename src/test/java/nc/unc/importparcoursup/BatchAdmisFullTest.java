package nc.unc.importparcoursup;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.PreCandidatRepository;
import nc.unc.importparcoursup.helper.AdmisIUTFileTestHelper;
import nc.unc.importparcoursup.helper.JobHelper;
import nc.unc.importparcoursup.utils.MyFileUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@SpringBootTest(classes = Application.class)
public class BatchAdmisFullTest {

    @Autowired
    @Qualifier("importAdmisJob")
    Job jobImportCsv;

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    PreCandidatRepository preCandidatRepo;

    @Value("${file.archive-directory}")
    private String outputDirectory;

    @Autowired
    JobRepository jobRepo;

    @Test
    public void testEmptyBaseOneLineFile() throws Exception {
        File file = new AdmisIUTFileTestHelper().createOneLineFileWithId(AdmisIUTFileTestHelper.DEFAULT_ID).getFile();

        JobHelper.launchJob(jobRepo, jobImportCsv, file);

        // file archive
        verifArchiveFile();

        // pre candidat save in base
        assertThat(preCandidatRepo.findById(AdmisIUTFileTestHelper.DEFAULT_ID)).isNotNull();
    }

    @Test
    public void preCandidatAlreadyExists() throws Exception {
        File file = new AdmisIUTFileTestHelper().createOneLineFile().getFile();
        JobHelper.launchJob(jobRepo, jobImportCsv, file);
        JobHelper.launchJob(jobRepo, jobImportCsv, file);
    }


    private void verifArchiveFile() {
        FileSystemResource outpuFileDir = new FileSystemResource(outputDirectory);
        Collection<File> resultCol = FileUtils.listFiles(outpuFileDir.getFile(), TrueFileFilter.INSTANCE,
                TrueFileFilter.INSTANCE);
        assertThat(resultCol.size()).isEqualTo(1);
        File resultFile = resultCol.stream().findFirst().get();
        assertThat(MyFileUtils.readFile(resultFile)).isEqualTo(AdmisIUTFileTestHelper.DEFAULT_ID + AdmisIUTFileTestHelper.LINE_CONTENT);
    }
}
