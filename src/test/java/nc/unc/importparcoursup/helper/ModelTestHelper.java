package nc.unc.importparcoursup.helper;

import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import nc.unc.importparcoursup.dao.candidatDAO.CandidatHistory;
import nc.unc.importparcoursup.model.UAI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class ModelTestHelper {
    private static final Logger log = LoggerFactory.getLogger(ModelTestHelper.class);

    private static Date today;
    private static Date yesterday;

    public ModelTestHelper() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        yesterday = cal.getTime();
        today = new Date();
    }

    private CandidatHistory createCandidatHistory() {
        return new CandidatHistory("testLogin", "20121212_1212_" + UAI.IUT.getName() + ".csv", "somechecksum");
    }

    public CandidatHistory createCandidatHistory(Date date) {
        CandidatHistory CandidatHistory = createCandidatHistory();
        CandidatHistory.setJobExecutionDate(date);
        return CandidatHistory;
    }

    public CandidatHistory createCandidatHistory(Long id, Date date) {
        CandidatHistory CandidatHistory = createCandidatHistory();
        CandidatHistory.setId(id);
        CandidatHistory.setJobExecutionDate(date);
        return CandidatHistory;
    }

    private AdmisHistory createAdmisHistory() {
        return new AdmisHistory("testLogin", "20121212_1212_" + UAI.IUT.getName() + ".csv", "somechecksum");
    }

    public AdmisHistory createAdmisHistory(Date date) {
        AdmisHistory admisHistory = createAdmisHistory();
        admisHistory.setJobExecutionDate(date);
        return admisHistory;
    }

    public AdmisHistory createAdmisHistory(Long id, Date date) {
        AdmisHistory admisHistory = createAdmisHistory();
        admisHistory.setId(id);
        admisHistory.setJobExecutionDate(date);
        return admisHistory;
    }

    private Admis createAdmis(Long id, AdmisHistory admisHistory) {
        Admis stu = new Admis();
        stu.setId(id);
        stu.setAdmisHistory(admisHistory);
        return stu;
    }

    public Admis createAdmis(AdmisHistory admisHistory) {
        long id = new Random().nextLong();
        log.info("Create admis - id: " + id);
        return createAdmis(id, admisHistory);
    }

    public Date yesterday() {
        return yesterday;
    }

    public Date today() {
        return today;
    }

}
