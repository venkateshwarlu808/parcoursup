package nc.unc.importparcoursup.bdd;

import com.icegreen.greenmail.junit.GreenMailRule;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nc.unc.importparcoursup.Application;
import nc.unc.importparcoursup.batch.mail.EmailService;
import nc.unc.importparcoursup.dao.admisDAO.AdmisRejet;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRejetRepository;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.*;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.*;
import nc.unc.importparcoursup.helper.AdmisIUTFileTestHelper;
import nc.unc.importparcoursup.helper.AdmisUNIVFileTestHelper;
import nc.unc.importparcoursup.helper.FileHelper;
import nc.unc.importparcoursup.helper.JobHelper;
import nc.unc.importparcoursup.model.TypeRejet;
import nc.unc.importparcoursup.model.UAI;
import nc.unc.importparcoursup.utils.ScolUtils;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration
@SpringBootTest(classes = Application.class)
public class CucumberSteps {
    private static final Logger log = LoggerFactory.getLogger(CucumberSteps.class);
    @Autowired
    @Qualifier("importAdmisJob")
    Job jobImportCsv;
    @Autowired
    JobRepository jobRepo;
    @Autowired
    PreCandidatRepository preCandidatRepo;
    @Autowired
    PreCandidatureRepository preCandidatureRepository;
    @Autowired
    AdmisRejetRepository admisRejetRepository;
    @Autowired
    ScolFormationSpecialisationRepository scolFormationSpecialisationRepository;
    @Autowired
    ScolFormationDiplomeRepository scolFormationDiplomeRepository;
    @Autowired
    ScolFormationHabilitationRepository scolFormationHabilitationRepository;
    @Autowired
    BacRepository bacRepository;
    @Autowired
    PaysRepository paysRepository;
    private File file;
    @Autowired
    private EmailService emailService;
    GreenMail greenMail;

    @Before
    public void before() {
        preCandidatRepo.deleteAll();
        preCandidatureRepository.deleteAll();
        scolFormationSpecialisationRepository.deleteAll();
        admisRejetRepository.deleteAll();
        greenMail = new GreenMail(ServerSetupTest.SMTP);
        greenMail.start();
        greenMail.setUser("test@test.com", "test@test.com", "pwd");
    }

    @After
    public void after() {
        greenMail.stop();
    }

    @Given("Les codes diplomes")
    public void creerCodeDiplome(DataTable dt) {
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        list.stream().forEach(map -> createDiplome(Long.parseLong(map.get("FSPN_KEY")), map.get("FDIP_CODE")));
    }

    @Given("Les codes bac")
    public void creerCodeBac(DataTable dt) {
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        list.stream().forEach(map -> bacRepository.save(new Bac(map.get("BAC_CODE"))));
    }

    @Given("Les codes pays")
    public void creerCodePays(DataTable dt) {
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        list.stream().forEach(map -> paysRepository.save(new Pays(map.get("PAYS_CODE"))));
    }

    @Given("Le fichier des admissions UNIV")
    public void leFichierDesAdmissionsUniv(DataTable dt) {
        createFile(dt, UAI.UNIV);
    }

    @Given("Le fichier des admissions IUT")
    public void leFichierDesAdmissionsIut(DataTable dt) {
        createFile(dt, UAI.IUT);
    }

    @When("Lancer le job integration parcoursup")
    public void launchJobWithFile() throws Throwable {
        JobHelper.launchJob(jobRepo, jobImportCsv, file);
    }

    @Then("^(\\d+) email\\(s\\) recut$")
    public void mailRecut(int nb){
        MimeMessage[] receivedMessages = greenMail.getReceivedMessages();
        AssertionsForClassTypes.assertThat(receivedMessages.length).isEqualTo(nb);
    }

    @Then("^Precandidat (\\d+) créé avec code diplome (.*)$")
    public void lePrecandidatExisteDansLaBasePrecandidat(long pcId, String fdipcode) {
        Optional<PreCandidat> preCandidat = preCandidatRepo.findById(pcId);
        assertThat(preCandidat).isNotNull();
        assertThat(preCandidat.isPresent()).isTrue();
        assertThat(preCandidat.get().getPreCandidature()).isNotNull();
        assertThat(preCandidat.get().getPreCandidature().getScolFormationSpecialisation().getFdipcode()).isEqualTo(fdipcode);
    }

    @Then("^Le precandidat (\\d+) s'appelle (.*)$")
    public void lePrecandidatSappelle(long pcId, String nom) {
        Optional<PreCandidat> preCandidat = preCandidatRepo.findById(pcId);
        assertThat(preCandidat).isNotNull();
        assertThat(preCandidat.isPresent()).isTrue();
        assertThat(preCandidat.get().getCAND_NOM()).isEqualTo(nom);
    }

    @Then("^Il y a (\\d+) precandidat\\(s\\) dans la base precandidat$")
    public void il_y_a_precandidat_dans_la_base_precandidat(int nb) {
        assertThat(preCandidatRepo.count()).isEqualTo(nb);
    }

    @Then("^Admis (.*) est en rejet (.*)")
    public void admisEstEnRejet(String id, String code_rejet) {
        assertThat(streamAllRejets().anyMatch(admisRejet -> admisHasId(id, admisRejet) && matchingCodeRejet(admisRejet.getTypeRejet(), code_rejet))).isTrue();
    }

    private boolean matchingCodeRejet(TypeRejet typeRejet, String codeRejet) {
        return codeRejet.equalsIgnoreCase(typeRejet.name());
    }

    private boolean admisHasId(String id, AdmisRejet admisRejet) {
        return id.equals(admisRejet.getAdmis().getCandNumero());
    }

    private Stream<AdmisRejet> streamAllRejets() {
        return StreamSupport.stream(admisRejetRepository.findAll().spliterator(), false);
    }

    @Then("Affiche les tables")
    public void afficheTables() {
        log.info(" ######## AFFICHAGE TEST DATA TABLE ########");
        preCandidatRepo.findAll().forEach(pc -> {
            log.info("# preCandidatRepo -numéro: {} -précandidature: {}", pc.getCAND_NUMERO(), pc.getPreCandidature());
        });
        preCandidatureRepository.findAll().forEach(pc -> {
            log.info("# preCandidatureRepository -CANU_KEY: {} -getScolFormationSpecialisation: {} -precandidat: {}", pc.getCANU_KEY(), pc.getScolFormationSpecialisation(), pc.getPreCandidat());
        });
        admisRejetRepository.findAll().forEach(pc -> {
            log.info("# admisRejetRepository -typeRejet: {} -admisNumero: {}", pc.getTypeRejet(), pc.getAdmis().getCandNumero());
        });
        scolFormationDiplomeRepository.findAll().forEach(pc -> {
            log.info("# scolFormationDiplomeRepository -getFdipcode: {}", pc.getFdipcode());
        });
        scolFormationHabilitationRepository.findAll().forEach(pc -> {
            log.info("# scolFormationHabilitationRepository -getFspnkey: {} -getFhabniveau: {} -getFhabouvert: {}", pc.getFspnkey(), pc.getFhabniveau(), pc.getFhabouvert());
        });
        scolFormationSpecialisationRepository.findAll().forEach(pc -> {
            log.info("# scolFormationSpecialisationRepository -getFdipcode: {} -getFspnkey: {} ", pc.getFdipcode(), pc.getFspnkey());
        });
        log.info(" ######## FIN ########");
    }

    private void createDiplome(Long fspnkey, String fdipcode) {
        ScolFormationDiplome sfd = new ScolFormationDiplome();
        sfd.setFdipcode(fdipcode);
        ScolFormationSpecialisation sfs = new ScolFormationSpecialisation();
        sfs.setFspnkey(fspnkey);
        sfs.setFdipcode(fdipcode);
        ScolFormationHabilitation sfh = new ScolFormationHabilitation();
        sfh.setFspnkey(fspnkey);
        sfh.setFannkey(ScolUtils.getCurrentScolYear());
        sfh.setFhabniveau(1);
        sfh.setFhabouvert("O");
        sfh.setFHAB_KEY(new Random().nextLong());
        scolFormationSpecialisationRepository.save(sfs);
        scolFormationDiplomeRepository.save(sfd);
        scolFormationHabilitationRepository.save(sfh);
    }

    private void createFile(DataTable dt, UAI uai) {
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        FileHelper afth = uai == UAI.UNIV ? new AdmisUNIVFileTestHelper().createEmptyFile() : new AdmisIUTFileTestHelper().createEmptyFile();
        list.stream().forEach(map -> {
            afth.addLineToFile(getLine(map, uai));
        });
        file = afth.getFile();
    }

    private String getLine(Map<String, String> map, UAI uai) {
        return map.get("candnumero") + "|" + "123" + "|" + "Mme" + "|" + map.get("nom") + "|TOTO|TOTO|01011950|Gy|45284|45|"+ map.get("PaysCodeNais") +"|100|TOTO|TOTO|17380|Gy|17448|100|06.|06.|a@g.f|" + map.get("baccode") + "|2017|0620042J|Lycée Gy|Gy|62|100||0333358W|IUT Gy|Gy|33|100|" + uai.getName() + "|" + map.get("fdipcode") + ";524";
    }
}
