package nc.unc.importparcoursup.utils;

import com.vaadin.flow.component.html.Label;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ViewUtilsTest {

    @Test
    public void checkedAndGreenTest() {
        Label label = new Label();
        ViewUtils.checkedAndGreen(label);
        assertThat(label.getText()).isEqualTo(SpecialCharUtils.CHECKED);
        assertThat(label.getClassName()).isEqualTo(ViewUtils.GREEN);
    }

    @Test
    public void uncheckedAndRedTest() {
        Label label = new Label();
        ViewUtils.uncheckedAndRed(label);
        assertThat(label.getText()).isEqualTo(SpecialCharUtils.CROSSED);
        assertThat(label.getClassName()).isEqualTo(ViewUtils.RED);
    }

    @Test
    public void redTest() {
        Label label = new Label();
        ViewUtils.red(label);
        assertThat(label.getClassName()).isEqualTo(ViewUtils.RED);
    }

    @Test
    public void greenTest() {
        Label label = new Label();
        ViewUtils.green(label);
        assertThat(label.getClassName()).isEqualTo(ViewUtils.GREEN);
    }

    @Test
    public void checkedTest() {
        Label label = new Label();
        ViewUtils.checked(label);
        assertThat(label.getText()).isEqualTo(SpecialCharUtils.CHECKED);
    }

    @Test
    public void uncheckedTest() {
        Label label = new Label();
        ViewUtils.unchecked(label);
        assertThat(label.getText()).isEqualTo(SpecialCharUtils.CROSSED);
    }
}