package nc.unc.importparcoursup.utils;

import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class ScolUtilsTest {

    @Test
    public void getPaysTest () {
        assertThat(ScolUtils.getPays("99100")).isEqualTo("100");
        assertThat(ScolUtils.getPays("100")).isEqualTo("100");
    }
}
